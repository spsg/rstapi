[Netapp Devnet](https://devnet.netapp.com/)

[Netapp DevNet RESTAPI](https://devnet.netapp.com/restapi.php)

[Netapp IO](https://netapp.io)

[RESTful API Status Codes Reference](https://restfulapi.net/http-status-codes/)

[ONTAP REST API Reference](https://library.netapp.com/ecmdocs/ECMLP2856304/html/index.html)

[Ansible Storage Modules Referene](https://docs.ansible.com/ansible/2.9/modules/list_of_storage_modules.html)

[Ansible Netapp Ontap Info Docs](https://docs.ansible.com/ansible/2.9/modules/na_ontap_info_module.html#na-ontap-info-module)

[Netapp Ansible Sample Playbook](https://github.com/ansible-collections/netapp.ontap/blob/main/playbooks/examples/na_ontap_pb_upgrade_firmware_with_vars_file.yml)

[Netapp Ontap Python Client Library Documentation](https://library.netapp.com/ecmdocs/ECMLP2879970/html/index.html#netapp-ontap)
