python -m pip install --upgrade pip
pip install ipykernel
pip install bash_kernel
python -m bash_kernel.install
pip install powershell_kernel
python -m powershell_kernel.install
pip install wheel
pip install sshkernel
python -m sshkernel install
