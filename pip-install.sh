#!/usr/bin/bash
python3 -m pip install --upgrade pip
pip install ipykernel
pip install bash_kernel
python3 -m bash_kernel.install
pip install powershell_kernel
python3 -m powershell_kernel.install
pip install wheel
pip install sshkernel
python3 -m sshkernel install
