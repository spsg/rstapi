#! /usr/bin/env python3.7

import requests 
import json 
import urllib3

urllib3.disable_warnings()

url = "https://cluster2.demo.netapp.com/api/storage/aggregates"

payload={}
headers = {
 'Content-type': 'application/hal+json',
 'Authorization': 'Basic YWRtaW46TmV0YXBwMSE='
}

response = requests.request("GET", url, headers=headers, data=payload, verify =False)

result = json.loads(response.text)

result = result.items()
result = list(result)
result = result[2]
result1 = {}
result1 = dict([result])

print("\nAggregate(",result1,")",sep='')
print("\n")
