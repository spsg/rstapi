$vsCodeExec = ($Env:ProgramFiles) + "\Microsoft VS Code\Bin\code.cmd"
$extensions = @(
    "donjayamanne.git-extension-pack"
    "humao.rest-client"
    "ms-python.python"
    "ms-vscode-remote.vscode-remote-extensionpack"
    "ms-vscode.powershell"
    "redhat.ansible"
    "yzhang.markdown-all-in-one"
) | SORT

$extensions | ForEach-Object {
    try {
        Invoke-Expression "& '$vsCodeExec' --install-extension $_ --force"
        Write-Host # New-Line
    } catch {
        $_
        Exit(1)
    }
}

Exit(0)